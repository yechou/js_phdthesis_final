\contentsline {chapter}{\@textofLoF }{iv}{}%
\contentsline {chapter}{\@textofLoT }{x}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 1:}Introduction}{1}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 2:}Theory background}{4}{}%
\contentsline {section}{\numberline {2.1}The Standard Model}{4}{}%
\contentsline {section}{\numberline {2.2}Neutrino Interactions}{7}{}%
\contentsline {section}{\numberline {2.3}Electron interactions}{10}{}%
\contentsline {section}{\numberline {2.4}Muon interactions}{11}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 3:}Experimental Apparatus}{13}{}%
\contentsline {section}{\numberline {3.1}The Large Hadron Collider}{13}{}%
\contentsline {section}{\numberline {3.2}The FASER Spectrometer}{14}{}%
\contentsline {section}{\numberline {3.3}The Interface Tracker}{21}{}%
\contentsline {section}{\numberline {3.4}The FASER$\nu $ Emulsion Detector}{21}{}%
\contentsline {section}{\numberline {3.5}The Pilot Detector}{27}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 4:}Tracker and Emulsion Datasets}{29}{}%
\contentsline {section}{\numberline {4.1}Tracker Performance Dataset}{29}{}%
\contentsline {section}{\numberline {4.2}Pilot Data Sample}{29}{}%
\contentsline {section}{\numberline {4.3}Pilot Detector MC Simulation Sample}{31}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 5:}Tracking and Reconstruction with FASER}{34}{}%
\contentsline {section}{\numberline {5.1}Tracking in FASER}{34}{}%
\contentsline {subsection}{\numberline {5.1.1}Digitization}{34}{}%
\contentsline {subsection}{\numberline {5.1.2}Clustering}{35}{}%
\contentsline {subsection}{\numberline {5.1.3}Space-Point Formation}{35}{}%
\contentsline {subsection}{\numberline {5.1.4}Track-finding}{37}{}%
\contentsline {subsection}{\numberline {5.1.5}Track-fitting with the combinatorial Kalman filter}{38}{}%
\contentsline {section}{\numberline {5.2}Estimation of tracker performance}{40}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 6:}Electron Clustering and Energy Reconstruction}{42}{}%
\contentsline {section}{\numberline {6.1}Muon Subtraction}{42}{}%
\contentsline {section}{\numberline {6.2}Electron clustering algorithm}{46}{}%
\contentsline {section}{\numberline {6.3}Electron energy regression}{50}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 7:}Pilot Run Analysis}{53}{}%
\contentsline {section}{\numberline {7.1}Comparison of raw basetrack parameters between MC and data}{55}{}%
\contentsline {section}{\numberline {7.2}Muon identification}{55}{}%
\contentsline {section}{\numberline {7.3}Electron clustering sample preparation}{58}{}%
\contentsline {section}{\numberline {7.4}Cluster purification in the pilot data}{62}{}%
\contentsline {section}{\numberline {7.5}Electron energy regression with the pilot data}{63}{}%
\contentsline {section}{\numberline {7.6}Comparison of clustered electron showers between MC and data}{67}{}%
\contentsline {section}{\numberline {7.7}Electron clustering}{72}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 8:}Results and Conclusion}{74}{}%
\contentsline {chapter}{\numberline {\@textofChapter \ 9:}Outlook}{76}{}%
\contentsline {chapter}{\@textofBIB }{78}{}%
\contentsline {chapter}{\numberline {\@textofAppendix \ A:}Helix Tracker Reconstruction using FaserMC}{82}{}%
\contentsline {subsection}{\numberline {A.0.1}Naive Track Fitting}{82}{}%
\contentsline {chapter}{\numberline {\@textofAppendix \ B:}Neutrino Signal / Background Signatures}{87}{}%
\contentsline {section}{\numberline {B.1}Electron neutrino CC events}{87}{}%
\contentsline {section}{\numberline {B.2}Muon neutrino CC events}{90}{}%
\contentsline {section}{\numberline {B.3}Tau neutrino CC events}{90}{}%
\contentsline {section}{\numberline {B.4}Background Processes}{91}{}%
\contentsline {chapter}{\numberline {\@textofAppendix \ C:}Truth-Leve Simulation Study of Electromagnetic Showers}{98}{}%
\contentsline {section}{\numberline {C.1}Characteristics of Electromagnetic Showers}{98}{}%
\contentsline {section}{\numberline {C.2}Electron Energy Reconstruction with Total Track Count}{99}{}%
\contentsline {section}{\numberline {C.3}Electron Energy Reconstruction with Maximum Track Depth and Multiplicity}{100}{}%
\contentsline {section}{\numberline {C.4}Electron Energy Reconstruction with the Full Shower Profile}{102}{}%
\contentsline {chapter}{\numberline {\@textofAppendix \ D:}Tracker and Tracker+IFT Performance}{106}{}%
\contentsline {section}{\numberline {D.1}Tracker performance}{109}{}%
\contentsline {chapter}{\numberline {\@textofAppendix \ E:}IFT Track Matching between FASER Tracker and FASER$\nu $ Emulsion Detector}{112}{}%
